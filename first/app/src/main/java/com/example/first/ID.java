package com.example.first;

public class ID {

    private String title;
    private String start ;
    private String end ;
    private String description ;

    public ID() {
    }

    public ID(String title,String start, String end, String description) {
        this.start = title ;
        this.start = start;
        this.end = end;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }


}
