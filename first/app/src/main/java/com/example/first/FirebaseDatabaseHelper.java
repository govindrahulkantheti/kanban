package com.example.first;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;

public class FirebaseDatabaseHelper {

    private FirebaseDatabase mDatabase ;
    private DatabaseReference mReferenceBooks;
    private List<ID> books = new ArrayList<>() ;

    public interface DataStatus{
        void DataIsLoaded(List<ID> books,List<String> keys);
        void DataIsInserted() ;
        void DataIsUpdated() ;
        void DataIsDeleted() ;
    }

    public FirebaseDatabaseHelper() {
        FirebaseAuth mAuth;
        mAuth = FirebaseAuth.getInstance();
        final FirebaseUser user = mAuth.getCurrentUser();

        String uid = user.getUid();

        mDatabase = FirebaseDatabase.getInstance() ;
        mReferenceBooks = mDatabase.getReference(user.getUid()) ;
    }

    public void readBooks(final DataStatus dataStatus)
    {
        mReferenceBooks.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            books.clear();
            List<String> keys = new ArrayList<>() ;
            for(DataSnapshot keyNode : dataSnapshot.getChildren())
            {
                keys.add(keyNode.getKey()) ;
                ID id = keyNode.getValue(ID.class);
                books.add(id) ;
            }
            dataStatus.DataIsLoaded(books,keys);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void addBook(ID id,final DataStatus dataStatus)
    {
        FirebaseAuth mAuth;
        mAuth = FirebaseAuth.getInstance();
        final FirebaseUser user = mAuth.getCurrentUser();

        //String key = user.getUid();
        String key = mReferenceBooks.push().getKey() ;
        mReferenceBooks.child(key).setValue(id).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                dataStatus.DataIsInserted();
            }
        }) ;
    }

}
