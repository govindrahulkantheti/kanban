package com.example.first;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class NewBookActivity extends AppCompatActivity {

    private TextView start ;
    private TextView end ;
    private EditText title ;
    private EditText description ;
    private Button add ;
    private Button back ;
    private CalendarView cal ;
    private TextView datetxt ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_book);
        title = findViewById(R.id.edittitle) ;
        start = findViewById(R.id.editstart) ;
        end = findViewById(R.id.editend) ;
        description = findViewById(R.id.editdesc) ;
        add = findViewById(R.id.addbtn) ;
        back = findViewById(R.id.backbtn) ;
        cal = findViewById(R.id.cal) ;
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cal.setVisibility(View.VISIBLE);
                cal.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
                    @Override
                    public void onSelectedDayChange(@NonNull CalendarView view, int i, int i1, int i2) {
                        String date = (i1+1) + "/" + (i2) + "/" + i ;
                        start.setText(date) ;
                        cal.setVisibility(View.GONE);
                    }
                });
            }
        });

        end.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cal.setVisibility(View.VISIBLE);
                cal.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
                    @Override
                    public void onSelectedDayChange(@NonNull CalendarView view, int i, int i1, int i2) {
                        String date = (i1+1) + "/" + (i2) + "/" + i ;
                        end.setText(date) ;
                        cal.setVisibility(View.GONE);
                    }
                });
            }
        });

       add.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               ID id = new ID() ;
               id.setTitle(title.getText().toString());
               id.setStart(start.getText().toString());
               id.setEnd(end.getText().toString());
               id.setDescription(description.getText().toString());


               new FirebaseDatabaseHelper().addBook(id, new FirebaseDatabaseHelper.DataStatus() {
                   @Override
                   public void DataIsLoaded(List<ID> books, List<String> keys) {

                   }

                   @Override
                   public void DataIsInserted() {
                       Toast.makeText(NewBookActivity.this,"Data Inserted",Toast.LENGTH_LONG).show();
                   }

                   @Override
                   public void DataIsUpdated() {

                   }

                   @Override
                   public void DataIsDeleted() {

                   }
               });
           }
       });

       back.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               finish(); return ;
           }
       });

    }

    @Override
    protected void onStart() {
        super.onStart();
        cal.setVisibility(View.GONE);
    }
}
