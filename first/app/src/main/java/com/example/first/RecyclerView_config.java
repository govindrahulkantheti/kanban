package com.example.first;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;
import java.util.Random;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerView_config {

    private Context mContext ;
    private BooksAdapter mBooksAdapter;
    public void setConfig(RecyclerView recyclerView,Context context,List<ID> ids,List<String> keys)
    {
        mContext = context ;
        mBooksAdapter = new BooksAdapter(ids,keys);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(mBooksAdapter);
    }

    class BookItemView extends RecyclerView.ViewHolder{
        private TextView title ;
        private TextView start ;
        private TextView end ;
        private TextView desc ;
        private CardView card ;

        private String key ;

        public BookItemView(ViewGroup parent)
        {
            super(LayoutInflater.from(mContext).inflate(R.layout.book_list_item,parent,false)) ;
            card = itemView.findViewById(R.id.card) ;
            card.setCardBackgroundColor(getRandomColorCode());
            title = itemView.findViewById(R.id.titletext) ;
            start = (TextView) itemView.findViewById(R.id.start) ;
            end = (TextView) itemView.findViewById(R.id.end) ;
            desc = (TextView) itemView.findViewById(R.id.desc) ;
        }
        public int getRandomColorCode(){

            Random random = new Random();

            return Color.argb(255, random.nextInt(256), random.nextInt(256),     random.nextInt(256));
        }

        public void bind(ID id,String key)
        {
            String t = id.getTitle();
            String s = id.getStart();
            String e = id.getEnd();
            String d = id.getDescription();
            title.append("Project Title : ");
            start.append("Start Date : ");
            end.append("End Date : ");
            desc.append("Description : ");
            title.append(t);
            start.append(s);
            end.append(e);
            desc.append(d);
            this.key = key ;
        }
    }
    class BooksAdapter extends RecyclerView.Adapter<BookItemView>
    {
        private List<ID> mBookList ;
        private List<String> mkeys ;

        public BooksAdapter(List<ID> mBookList, List<String> mkeys) {
            this.mBookList = mBookList;
            this.mkeys = mkeys;
        }

        @NonNull
        @Override
        public BookItemView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new BookItemView(parent);
        }

        @Override
        public void onBindViewHolder(@NonNull BookItemView holder, int position) {
            holder.bind(mBookList.get(position),mkeys.get(position));
        }

        @Override
        public int getItemCount() {
            return mBookList.size();
        }
    }

}
