package com.example.first;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

public class personal extends AppCompatActivity {

    Button adddata,backbtn ;

    private RecyclerView mRecyclerView ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal);
        mRecyclerView = findViewById(R.id.recyclerView) ;
        new FirebaseDatabaseHelper().readBooks(new FirebaseDatabaseHelper.DataStatus() {
            @Override
            public void DataIsLoaded(List<ID> books, List<String> keys) {
                findViewById(R.id.pb).setVisibility(View.GONE);
                new RecyclerView_config().setConfig(mRecyclerView,personal.this,books,keys);
            }

            @Override
            public void DataIsInserted() {

            }

            @Override
            public void DataIsUpdated() {

            }

            @Override
            public void DataIsDeleted() {

            }
        });

        click() ;
    }

   private void click()
   {
       adddata = findViewById(R.id.adddatabtn) ;
       backbtn = findViewById(R.id.backbtn);

       adddata.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent i = new Intent(personal.this,NewBookActivity.class) ;
               startActivity(i) ;
           }
       });

       backbtn.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               finish(); return ;
           }
       });

   }

}