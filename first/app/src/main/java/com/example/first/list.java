package com.example.first;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.google.firebase.database.core.view.View;

import java.util.List;

public class list extends ArrayAdapter<Data> {

    private Activity context;
    private List<Data> datalist;

    public list(Activity context, List<Data> datalist) {
        super(context, R.layout.list, datalist);
        this.context = context;
        this.datalist = datalist;
    }

    public android.view.View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        android.view.View listViewItem = inflater.inflate(R.layout.list,null,true );
        TextView list = listViewItem.findViewById(R.id.list) ;

        Data data = datalist.get(position) ;

        list.setText(data.getName());

        return listViewItem ;

    }

}
